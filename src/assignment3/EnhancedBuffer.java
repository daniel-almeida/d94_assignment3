package assignment3;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import util.*;

/**
 * private <T extends Number> T read(Class<T> c, int index) private <T extends
 * Number> ArrayList<T> read(Class<T> c, int numberOfElements, int index)
 * private <T extends Number> boolean write(Class<T> c, int index, T value)
 * private <T extends Number> boolean write(Class<T> c, int index, ArrayList<T>
 * values)
 **/


public class EnhancedBuffer {

	/*
	 * rawBuffer is a regular byte array which is "wrapped" by ByteBuffer. 
	 * The ideal should be to implement all available functions on ByteBuffer
	 * put<Type> and get<Type>.
	 */
	
	private static byte[] rawBuffer = null;
	public static ByteBuffer wrapperBuffer = null;

	public EnhancedBuffer(int capacity) {
		rawBuffer = new byte[capacity];
		wrapperBuffer = ByteBuffer.wrap(rawBuffer);
	}

	private U8 readU8(int index) {
		return new U8(wrapperBuffer.get(index));
	}

	private boolean writeU8(int index, U8 u8) {
		wrapperBuffer.put(index, (byte) (u8.getValue() & 0xFF));
		return true;
	}

	private I8 readI8(int index) {
		return new I8(wrapperBuffer.get(index));
	}

	private boolean writeI8(int index, I8 i8) {
		wrapperBuffer.put(index, i8.getValue());
		return true;
	}

	private U16 readU16(int index) {
		return new U16(wrapperBuffer.getShort(index));
	}

	private boolean writeU16(int index, U16 u16) {
		wrapperBuffer.putShort(index, (short) (u16.getValue() & 0xFFFF));
		return true;
	}

	private I16 readI16(int index) {
		return new I16(wrapperBuffer.getShort(index));
	}

	private boolean writeI16(int index, I16 i16) {
		wrapperBuffer.putShort(index, i16.getValue());
		return true;
	}

	private I32 readI32(int index) {
		return new I32(wrapperBuffer.getInt(index));
	}

	private boolean writeI32(int index, I32 i32) {
		wrapperBuffer.putInt(index, i32.getValue());
		return true;
	}

	private U32 readU32(int index) {
		return new U32(wrapperBuffer.getInt(index));
	}

	private boolean writeU32(int index, U32 u32) {
		wrapperBuffer.putInt(index, (int) (u32.getValue() & 0xFFFFFFFFL));
		return true;
	}

	/*
	 * Read an object of type T from the buffer beginning at index. 
	 * Only reads primitive types that were annotated as @PrimitiveField.
	 */

	public <T> T readObject(Class<T> c, int index, boolean littleEndian) throws IllegalArgumentException, IllegalAccessException, InstantiationException{
		Field[] fieldsList = c.getDeclaredFields();
		String fieldName = "";
		int currentPosition = index;
		int sizeOfArray;
		
		T ret = c.newInstance();
		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		for (Field field : fieldsList) {
			if (field.isAnnotationPresent(PrimitiveField.class)) { // Single primitive field
				fieldName = field.getType().getName();
				field.setAccessible(true);
				switch (fieldName) {
				case "util.I8":
					I8 i8temp;
					i8temp = this.readI8(currentPosition);
					field.set(ret, i8temp);
					currentPosition += 1;
					break;
				case "util.U8":
					U8 u8temp;
					u8temp = this.readU8(currentPosition);
					field.set(ret, u8temp);
					currentPosition += 1;
					break;
				case "util.I16":
					I16 i16temp;
					i16temp = this.readI16(currentPosition);
					field.set(ret, i16temp);
					currentPosition += 2;
					break;
				case "util.U16":
					U16 u16temp;
					u16temp = this.readU16(currentPosition);
					field.set(ret, u16temp);
					currentPosition += 2;
					break;
				case "util.I32":
					I32 i32temp;
					i32temp = this.readI32(currentPosition);
					field.set(ret, i32temp);
					currentPosition += 4;
					break;
				case "util.U32":
					U32 u32temp;
					u32temp = this.readU32(currentPosition);
					field.set(ret, u32temp);
					currentPosition += 4;
					break;
				}
			} else { // Array of primitive fields
				if (field.isAnnotationPresent(PrimitiveFieldArray.class)) {
					String type = field.getAnnotation(PrimitiveFieldArray.class).type().getSimpleName();
					sizeOfArray = field.getAnnotation(PrimitiveFieldArray.class).size();
					field.setAccessible(true);
					switch (type) {
					case "I8":
						ArrayList<I8> i8temp = new ArrayList<I8>(sizeOfArray);
						for (int i = 0; i < sizeOfArray; i++) {
							i8temp.add(this.readI8(currentPosition));
							currentPosition += 1;
						}
						field.set(ret, i8temp);			
						break;
					case "U8":
						ArrayList<U8> u8temp = new ArrayList<U8>(sizeOfArray);
						for (int i = 0; i < sizeOfArray; i++) {
							u8temp.add(this.readU8(currentPosition));
							currentPosition += 1;
						}
						field.set(ret, u8temp);			
						break;
					case "I16":
						ArrayList<I16> i16temp = new ArrayList<I16>(sizeOfArray);
						for (int i = 0; i < sizeOfArray; i++) {
							i16temp.add(this.readI16(currentPosition));
							currentPosition += 2;
						}
						field.set(ret, i16temp);			
						break;
					case "U16":
						ArrayList<U16> u16temp = new ArrayList<U16>(sizeOfArray);
						for (int i = 0; i < sizeOfArray; i++) {
							u16temp.add(this.readU16(currentPosition));
							currentPosition += 2;
						}
						field.set(ret, u16temp);			
						break;
					case "I32":
						ArrayList<I32> i32temp = new ArrayList<I32>(sizeOfArray);
						for (int i = 0; i < sizeOfArray; i++) {
							i32temp.add(this.readI32(currentPosition));
							currentPosition += 4;
						}
						field.set(ret, i32temp);			
						break;
					case "U32":
						ArrayList<U32> u32temp = new ArrayList<U32>(sizeOfArray);
						for (int i = 0; i < sizeOfArray; i++) {
							u32temp.add(this.readU32(currentPosition));
							currentPosition += 4;
						}
						field.set(ret, u32temp);			
						break;
					}
				}
			}
		}

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.BIG_ENDIAN);
		}
		return ret;
	}
	
	
	/*
	 * Write a generic object of type T which contains n primitive types (U8, I16, U32, ...) 
	 * as long as they are annotated as @PrimitiveField.
	 */
	@SuppressWarnings("unchecked")
	public <T> boolean writeObject(Class<T> c, int index, T value, boolean littleEndian) throws Exception {

		Field[] fieldsList = c.getDeclaredFields();
		String fieldName = "";
		int currentPosition = index;
		int sizeOfArray;

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		for (Field field : fieldsList) {
			if (field.isAnnotationPresent(PrimitiveField.class)) {
				fieldName = field.getType().getName();
				field.setAccessible(true);
				switch (fieldName) {
				case "util.I8":
					I8 i8temp;
					i8temp = (I8) field.get(value);
					this.writeI8(currentPosition, i8temp);
					currentPosition += 1;
					break;
				case "util.U8":
					U8 u8temp;
					u8temp = (U8) field.get(value);
					this.writeU8(currentPosition, u8temp);
					currentPosition += 1;
					break;
				case "util.I16":
					I16 i16temp;
					i16temp = (I16) field.get(value);
					this.writeI16(currentPosition, i16temp);
					currentPosition += 2;
					break;
				case "util.U16":
					U16 u16temp;
					u16temp = (U16) field.get(value);
					this.writeU16(currentPosition, u16temp);
					currentPosition += 2;
					break;
				case "util.I32":
					I32 i32temp;
					i32temp = (I32) field.get(value);
					this.writeI32(currentPosition, i32temp);
					currentPosition += 4;
					break;
				case "util.U32":
					U32 u32temp;
					u32temp = (U32) field.get(value);
					this.writeU32(currentPosition, u32temp);
					currentPosition += 4;
					break;
				}
			} else {
				if (field.isAnnotationPresent(PrimitiveFieldArray.class)) {
					String type = field.getAnnotation(PrimitiveFieldArray.class).type().getSimpleName();
					sizeOfArray = field.getAnnotation(PrimitiveFieldArray.class).size();
					field.setAccessible(true);
					switch (type) {
					case "I8":
						ArrayList<I8> i8temp = new ArrayList<I8>(sizeOfArray);
						i8temp = (ArrayList<I8>) field.get(value);
						for (int i = 0; i < sizeOfArray; i++) {
							this.writeI8(currentPosition, i8temp.get(i));
							currentPosition += 1;
						}
						break;
					case "U8":
						ArrayList<U8> u8temp = new ArrayList<U8>(sizeOfArray);
						u8temp = (ArrayList<U8>) field.get(value);
						for (int i = 0; i < sizeOfArray; i++) {
							this.writeU8(currentPosition, u8temp.get(i));
							currentPosition += 1;
						}
						break;
					case "I16":
						ArrayList<I16> i16temp = new ArrayList<I16>(sizeOfArray);
						i16temp = (ArrayList<I16>) field.get(value);
						for (int i = 0; i < sizeOfArray; i++) {
							this.writeI16(currentPosition, i16temp.get(i));
							currentPosition += 2;
						}
						break;
					case "U16":
						ArrayList<U16> u16temp = new ArrayList<U16>(sizeOfArray);
						u16temp = (ArrayList<U16>) field.get(value);
						for (int i = 0; i < sizeOfArray; i++) {
							this.writeU16(currentPosition, u16temp.get(i));
							currentPosition += 2;
						}
						break;
					case "I32":
						ArrayList<I32> i32temp = new ArrayList<I32>(sizeOfArray);
						i32temp = (ArrayList<I32>) field.get(value);
						for (int i = 0; i < sizeOfArray; i++) {
							this.writeI32(currentPosition, i32temp.get(i));
							currentPosition += 4;
						}
						break;
					case "U32":
						ArrayList<U32> u32temp = new ArrayList<U32>(sizeOfArray);
						u32temp = (ArrayList<U32>) field.get(value);
						for (int i = 0; i < sizeOfArray; i++) {
							this.writeU32(currentPosition, u32temp.get(i));
							currentPosition += 4;
						}
						break;
					}
				}
			}
		}
		
		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.BIG_ENDIAN);
		}

		return true;
		
	}
	
	
	/* END OF NEW TASK */

	// Read function for single primitives
	@SuppressWarnings("unchecked")
	public <T> T read(Class<T> c, int index, boolean littleEndian){
		T ret = null;
		String type = c.getCanonicalName();

		/*
		 * Absolute get doesn't update limit and this probably wouldn't make
		 * sense since we are changing arbitrary pieces of the rawBuffer.
		 * Therefore, we only set the limit to the max capacity of the
		 * rawBuffer.
		 */

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		wrapperBuffer.flip();
		int maxLimit = wrapperBuffer.capacity();
		wrapperBuffer.limit(maxLimit);

		switch (type) {
		case "util.I8":
			ret =  (T) this.readI8(index);
			break;
		case "util.U8":
			ret =  (T) this.readU8(index);
			break;
		case "util.I16":
			ret =  (T) this.readI16(index);
			break;
		case "util.U16":
			ret =  (T) this.readU16(index);
			break;
		case "util.I32":
			ret =  (T) this.readI32(index);
			break;
		case "util.U32":
			ret =  (T) this.readU32(index);
			break;
		}
		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.BIG_ENDIAN);
		}
		return ret;
	}

	// Read version for array operations
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> read(Class<T> c,
			int numberOfElements, int index, boolean littleEndian) {

		// Create array of bytes of size numberOfElements
		ArrayList<T> tempArray = new ArrayList<T>(numberOfElements);
		String type = c.getCanonicalName();

		/*
		 * Absolute get doesn't update limit and this probably wouldn't make
		 * sense since we are changing arbitrary pieces of the rawBuffer.
		 * Therefore, we only set the limit to the max capacity of the
		 * rawBuffer.
		 */

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		wrapperBuffer.flip();
		int maxLimit = wrapperBuffer.capacity();
		wrapperBuffer.limit(maxLimit);

		switch (type) {
		case "util.I8":
			for (int i = 0; i < numberOfElements; i++) {
				tempArray.add((T) new I8(this.readI8(index).getValue()));
				index++;
			}
			break;
		case "util.U8":
			for (int i = 0; i < numberOfElements; i++) {
				tempArray.add((T) new U8(this.readU8(index).getValue()));
				index++;
			}
			break;
		case "util.I16":
			for (int i = 0; i < numberOfElements; i++) {
				tempArray.add((T) new I16(this.readI16(index).getValue()));
				index += 2;
			}
			break;
		case "util.U16":
			for (int i = 0; i < numberOfElements; i++) {
				tempArray.add((T) new U16(this.readU16(index).getValue()));
				index += 2;
			}
			break;
		case "util.I32":
			for (int i = 0; i < numberOfElements; i++) {
				tempArray.add((T) new I32(this.readI32(index).getValue()));
				index += 4;
			}
			break;
		case "util.U32":
			for (int i = 0; i < numberOfElements; i++) {
				tempArray.add((T) new U32(this.readU32(index).getValue()));
				index += 4;
			}
			break;
		}

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.BIG_ENDIAN);
		}

		return tempArray;
	}

	// Write function for single primitives
	public <T> boolean write(Class<T> c, int index, T value, boolean littleEndian) {
		String type = c.getCanonicalName();

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		switch (type) {
		case "util.I8":
			this.writeI8(index, (I8) value);
			break;
		case "util.U8":
			this.writeU8(index, (U8) value);
			break;
		case "util.I16":
			this.writeI16(index, (I16) value);
			break;
		case "util.U16":
			this.writeU16(index, (U16) value);
			break;
		case "util.I32":
			this.writeI32(index, (I32) value);
			break;
		case "util.U32":
			this.writeU32(index, (U32) value);
			break;
		}


		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.BIG_ENDIAN);
		}

		return true;
	}

	// Write version for array operations
	public <T> boolean write(Class<T> c, int index,
			ArrayList<T> values, boolean littleEndian) {

		int numberOfElements = values.size();
		String type = c.getCanonicalName();

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.LITTLE_ENDIAN);
		}

		switch (type) {
		case "util.I8":
			for (int i = 0; i < numberOfElements; i++) {
				this.writeI8(index, (I8) values.get(i));
				index++;
			}
			break;
		case "util.U8":
			for (int i = 0; i < numberOfElements; i++) {
				this.writeU8(index, (U8) values.get(i));
				index++;
			}
			break;
		case "util.I16":
			for (int i = 0; i < numberOfElements; i++) {
				this.writeI16(index, (I16) values.get(i));
				index += 2;
			}
			break;
		case "util.U16":
			for (int i = 0; i < numberOfElements; i++) {
				this.writeU16(index, (U16) values.get(i));
				index += 2;
			}
			break;
		case "util.I32":
			for (int i = 0; i < numberOfElements; i++) {
				this.writeI32(index, (I32) values.get(i));
				index += 4;
			}
			break;
		case "util.U32":
			for (int i = 0; i < numberOfElements; i++) {
				this.writeU32(index, (U32) values.get(i));
				index += 4;
			}
			break;
		}

		if (littleEndian) {
			wrapperBuffer.order(ByteOrder.BIG_ENDIAN);
		}

		return true;
	}


}