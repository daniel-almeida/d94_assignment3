package assignment3;

import util.*;

public class EnhacedBufferTester {

	public static void main(String[] args) {

		EnhancedBuffer buf = new EnhancedBuffer(300);

		// TESTING NEW FUNCTIONS

		//		byte b = 10;
		//		short s = 127;
		//		int i = 300;
		//
		//		int bb = 141;
		//		short ss = 16;
		//		int ii = 345;
		//		long ll = 222333;
		//		
		//		I8 testByte = new I8(b);
		//		U8 testUByte = new U8(bb);
		//		
		//		I16 testShort = new I16(s);
		//		U16 testUShort = new U16(ss);
		//		
		//		I32 testInt = new I32(i);
		//		U32 testUInt = new U32(ll);

		/* Writing single primitives to rawBuffer */
		//		buf.write(I8.class, 2, testByte, false);
		//		buf.write(U8.class, 4, testUByte, false);
		//		buf.write(U8.class, 6, testUByte, false);
		//		buf.write(I16.class, 15, testShort, false);
		//		buf.write(U16.class, 20, testUShort, false);
		//		buf.write(I32.class, 30, testInt, false);
		//		buf.write(U32.class, 50, testUInt, false);
		//		
		//		I8 returnI8 = null;
		//		U8 returnU8 = null;
		//		I16 returnI16 = null;
		//		U16 returnU16 = null;
		//		I32 returnI32 = null;
		//		U32 returnU32 = null;
		//		
		//		/* Reading single primitives from wrapperBufferer */
		//		returnI8 = buf.read(I8.class, 2, false);
		//		System.out.println("byte is " + returnI8.getValue());
		//		returnU8 = buf.read(U8.class, 4, false);
		//		System.out.println("first Unsigned byte is " + returnU8.getValue());
		//		returnU8 = buf.read(U8.class, 6, false);
		//		System.out.println("second Unsigned byte is " + returnU8.getValue());
		//		returnI16 = buf.read(I16.class, 15, false);
		//		System.out.println("short is " + returnI16.getValue());
		//		returnU16 = buf.read(U16.class, 20, false);
		//		System.out.println("short is " + returnU16.getValue());
		//		returnI32 = buf.read(I32.class, 30, false);
		//		System.out.println("integer is " + returnI32.getValue());
		//		returnU32 = buf.read(U32.class, 50, false);
		//		System.out.println("integer is " + returnU32.getValue());

		/* Write an array Byte */
		//		ArrayList<I8> byteArrayWrite = new ArrayList<I8>(5);
		//		for (int cont = 0; cont < 5; cont++) {
		//			byteArrayWrite.add(cont, testByte);
		//		}
		//		buf.write(I8.class, 100, byteArrayWrite, false);
		//
		//		/* Reading an array Byte */
		//		ArrayList<I8> byteArray = new ArrayList<I8>(5);
		//		byteArray = buf.read(I8.class, 5, 100, false);
		//		for (int cont = 0; cont < byteArray.size(); cont++) {
		//			System.out.println("Byte " + cont + ": " + byteArray.get(cont).getValue());
		//		}
		//
		//		/* Write an array Integer */
		//		ArrayList<U32> intArrayWrite = new ArrayList<U32>(5);
		//		for (int cont = 0; cont < 5; cont++) {
		//			intArrayWrite.add(cont, testUInt);
		//		}
		//		buf.write(U32.class, 130, intArrayWrite, false);
		//
		//		/* Reading an array Integer */
		//		ArrayList<U32> intArray = new ArrayList<U32>(5);
		//		intArray = buf.read(U32.class, 5, 130, false);
		//		for (int cont = 0; cont < intArray.size(); cont++) {
		//			System.out.println("Int " + cont + ": " + intArray.get(cont).getValue());
		//		}

		// Testing Endianness

		// Testing using bytes -> integers

		byte b1 = 12;
		byte b2 = 14;
		byte b3 = 16;
		byte b4 = 18;

		int bb1 = 130;
		int bb2 = 145;
		int bb3 = 175;
		int bb4 = 199;

		short sh = 32000;

		I8 testByte1 = new I8(b1);
		I8 testByte2 = new I8(b2);
		I8 testByte3 = new I8(b3);
		I8 testByte4 = new I8(b4);

		U8 testUByte1 = new U8(bb1);
		U8 testUByte2 = new U8(bb2);
		U8 testUByte3 = new U8(bb3);
		U8 testUByte4 = new U8(bb4);
		I32 testI32 = new I32(2147483647);
		U32 testU32 = new U32(4294967295L);
		I16 testI16 = new I16(sh);
		U16 testU16 = new U16(35000);

		buf.write(I8.class, 170, testByte1, false);
		buf.write(I8.class, 171, testByte2, false);
		buf.write(I8.class, 172, testByte3, false);
		buf.write(I8.class, 173, testByte4, false);

		buf.write(U8.class, 160, testUByte1, false);
		buf.write(I32.class, 180, testI32, false);
		buf.write(U32.class, 185, testU32, false);
		buf.write(I16.class, 190, testI16, false);
		buf.write(U16.class, 250, testU16, false);

		I8 signedByte = buf.read(I8.class, 160, false);
		U8 unsignedByte = buf.read(U8.class, 160, false);
		System.out.println("Signed Byte " + signedByte.getValue());
		System.out.println("Unsigned Byte " + unsignedByte.getValue());

		I16 signedShort = buf.read(I16.class, 190, false);
		U16 unsignedShort = buf.read(U16.class, 250, false);
		System.out.println("Signed Short " + signedShort.getValue());
		System.out.println("Unsigned Short " + unsignedShort.getValue());

		I32 signedInt = buf.read(I32.class, 180, false);
		U32 unsignedInt = buf.read(U32.class, 185, false);
		System.out.println("Signed Int " + signedInt.getValue());
		System.out.println("Unsigned Int " + unsignedInt.getValue());


		I32 intSignedBig = buf.read(I32.class, 170, false);
		I32 intSignedLittle = buf.read(I32.class, 170, true);
		buf.write(U8.class, 174, testUByte1, false);
		buf.write(U8.class, 175, testUByte2, false);
		buf.write(U8.class, 176, testUByte3, false);
		buf.write(U8.class, 177, testUByte4, false);
		U32 intUnsignedBig = buf.read(U32.class, 174, false);
		U32 intUnsignedLittle = buf.read(U32.class, 174, true);

		System.out.println("Signed Int Big Endian " + intSignedBig.getValue());
		System.out.println("Signed Int Big Endian as hexString is " + Integer.toHexString(intSignedBig.getValue()));
		System.out.println("Signed Int Little Endian " + intSignedLittle.getValue());
		System.out.println("Signed Int Little Endian as hexString is " + Integer.toHexString(intSignedLittle.getValue()));

		System.out.println("Unsigned Int Big Endian is " + intUnsignedBig.getValue());
		System.out.println("Unsigned Int Big Endian as hexString is " + Long.toHexString(intUnsignedBig.getValue()));
		System.out.println("Unsigned Int Little Endian is " + intUnsignedLittle.getValue());
		System.out.println("Unsigned Int Little Endian as hexString is " + Long.toHexString(intUnsignedLittle.getValue()));


		//		buf.write(I8.class, 150, testByte, false);
		//		buf.write(U8.class, 155, testUByte, false);
		//		Long longBig = buf.read(long.class, 110, false);
		//		
		//		buf.write(int.class, 160, i1, true);
		//		buf.write(int.class, 165, i2, true);
		//		Long longLittle = buf.read(long.class, 118, true);
		//		
		//		System.out.println("Long Big Endian is " + longBig);
		//		System.out.println("longBig as hexString is " + Long.toHexString(longBig));
		//		
		//		System.out.println("Long Little Endian is " + longLittle);
		//		System.out.println("longLittle as hexString is " + Long.toHexString(longLittle));

		//		short sTeste = 250;
		//		I16 testI16 = new I16(sTeste);
		//		buf.write(I16.class, 180, testI16, false);
		//		
		//		I16 returnI16 = buf.read(I16.class, 180, false);
		//		System.out.println("I16 is " + returnI16.getValue());
		//		
		//		U16 testU16 = new U16(130);
		//		buf.write(U16.class, 185, testU16, false);
		//		
		//		U16 returnU16 = buf.read(U16.class, 185, false);
		//		System.out.println("U16 is " + returnU16.getValue());
		//		
		//U32 returnU32 = buf.read(U32.class, 180, false);
		//System.out.println("I32 is " + returnU32.getValue());




		/* END OF TESTING SECTION */

	}

}
