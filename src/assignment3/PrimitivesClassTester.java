package assignment3;

import java.util.ArrayList;

import util.*;

/*
 * Class containing primitive types (U8, I16, U32, ...) to test readObject and writeObject. 
 * One instance of PrimitivesClassTester is instantiated, its values set and it's written 
 * to buffer using writeObject. Then a new null PrimitivesClassTester receives the result 
 * of the readObject method.
 */

public class PrimitivesClassTester {

	@PrimitiveField
	U8 u8Field;
	@PrimitiveField
	I8 i8Field;
	@PrimitiveField
	U16 u16Field;
	@PrimitiveField
	I16 i16Field;
	@PrimitiveField
	U32 u32Field;
	@PrimitiveField
	I32 i32Field;
	
	@PrimitiveFieldArray(size = 3, type = I8.class)
	ArrayList<I8> i8FieldArray = new ArrayList<I8>(3);
	
	@PrimitiveFieldArray(size = 4, type = U16.class)
	ArrayList<U16> u16FieldArray = new ArrayList<U16>(4);

	@PrimitiveFieldArray(size = 5, type = I32.class)
	ArrayList<I32> i32FieldArray = new ArrayList<I32>(5);
	
	public ArrayList<U16> getU16FieldArray() {
		return u16FieldArray;
	}

	public void setU16FieldArray(ArrayList<U16> u16FieldArray) {
		this.u16FieldArray = u16FieldArray;
	}

	public ArrayList<I32> getI32FieldArray() {
		return i32FieldArray;
	}

	public void setI32FieldArray(ArrayList<I32> i32FieldArray) {
		this.i32FieldArray = i32FieldArray;
	}

	public ArrayList<I8> getI8FieldArray() {
		return i8FieldArray;
	}

	public void setI8FieldArray(ArrayList<I8> i8FieldArray) {
		this.i8FieldArray = i8FieldArray;
	}

	public U8 getU8Field() {
		return u8Field;
	}

	public void setU8Field(U8 u8Field) {
		this.u8Field = u8Field;
	}

	public I8 getI8Field() {
		return i8Field;
	}

	public void setI8Field(I8 i8Field) {
		this.i8Field = i8Field;
	}

	public U16 getU16Field() {
		return u16Field;
	}

	public void setU16Field(U16 u16Field) {
		this.u16Field = u16Field;
	}

	public I16 getI16Field() {
		return i16Field;
	}

	public void setI16Field(I16 i16Field) {
		this.i16Field = i16Field;
	}

	public U32 getU32Field() {
		return u32Field;
	}

	public void setU32Field(U32 u32Field) {
		this.u32Field = u32Field;
	}

	public I32 getI32Field() {
		return i32Field;
	}

	public void setI32Field(I32 i32Field) {
		this.i32Field = i32Field;
	}

	int integ;
	String myName = "myName";

	public static void main(String[] args) throws Exception {		
		EnhancedBuffer buf = new EnhancedBuffer(500);
		PrimitivesClassTester tClassWrite = new PrimitivesClassTester();
		PrimitivesClassTester tClassRead = new PrimitivesClassTester();
		byte b = 15;
		byte b2 = 10;
		byte b3 = 5;
		short s = 200;
		ArrayList<I8> i8ArrayWrite = new ArrayList<I8>(3);
		i8ArrayWrite.add(new I8(b3));
		i8ArrayWrite.add(new I8(b2));
		i8ArrayWrite.add(new I8(b));
		
		ArrayList<U16> u16ArrayWrite = new ArrayList<U16>(4);
		u16ArrayWrite.add(new U16(500));
		u16ArrayWrite.add(new U16(1000));
		u16ArrayWrite.add(new U16(1500));
		u16ArrayWrite.add(new U16(2000));
		
		ArrayList<I32> i32ArrayWrite = new ArrayList<I32>(5);
		i32ArrayWrite.add(new I32(30));
		i32ArrayWrite.add(new I32(60));
		i32ArrayWrite.add(new I32(90));
		i32ArrayWrite.add(new I32(120));
		i32ArrayWrite.add(new I32(150));
		
		tClassWrite.setU8Field(new U8(10));
		tClassWrite.setI8Field(new I8(b));
		tClassWrite.setU16Field(new U16(35000));
		tClassWrite.setI16Field(new I16(s));
		tClassWrite.setU32Field(new U32(4294967295L));
		tClassWrite.setI32Field(new I32(2147483647));
		tClassWrite.setI8FieldArray(i8ArrayWrite);
		tClassWrite.setU16FieldArray(u16ArrayWrite);
		tClassWrite.setI32FieldArray(i32ArrayWrite);
		
		System.out.println("U8 being written is " + tClassWrite.getU8Field().getValue());
		System.out.println("I8 being written is " + tClassWrite.getI8Field().getValue());
		System.out.println("U16 being written is " + tClassWrite.getU16Field().getValue());
		System.out.println("I16 being written is " + tClassWrite.getI16Field().getValue());
		System.out.println("U32 being written is " + tClassWrite.getU32Field().getValue());
		System.out.println("I32 being written is " + tClassWrite.getI32Field().getValue());
		System.out.println("I8Array(0) being written is " + tClassWrite.getI8FieldArray().get(0).getValue());
		System.out.println("I8Array(1) being written is " + tClassWrite.getI8FieldArray().get(1).getValue());
		System.out.println("I8Array(2) being written is " + tClassWrite.getI8FieldArray().get(2).getValue());
		
		System.out.println("U16Array(0) being written is " + tClassWrite.getU16FieldArray().get(0).getValue());
		System.out.println("U16Array(1) being written is " + tClassWrite.getU16FieldArray().get(1).getValue());
		System.out.println("U16Array(2) being written is " + tClassWrite.getU16FieldArray().get(2).getValue());
		System.out.println("U16Array(3) being written is " + tClassWrite.getU16FieldArray().get(3).getValue());
		
		System.out.println("I32Array(0) being written is " + tClassWrite.getI32FieldArray().get(0).getValue());
		System.out.println("I32Array(1) being written is " + tClassWrite.getI32FieldArray().get(1).getValue());
		System.out.println("I32Array(2) being written is " + tClassWrite.getI32FieldArray().get(2).getValue());
		System.out.println("I32Array(3) being written is " + tClassWrite.getI32FieldArray().get(3).getValue());
		System.out.println("I32Array(4) being written is " + tClassWrite.getI32FieldArray().get(4).getValue());
		
		buf.writeObject(PrimitivesClassTester.class, 0, tClassWrite, false);
		System.out.println("Finished writing. :)");
		tClassRead = null;
		tClassRead = buf.readObject(PrimitivesClassTester.class, 0, false);
		System.out.println("Finished reading. :)");
		
		System.out.println("U8 read is " + tClassRead.getU8Field().getValue());
		System.out.println("I8 read is " + tClassRead.getI8Field().getValue());
		System.out.println("U16 read is " + tClassRead.getU16Field().getValue());
		System.out.println("I16 read is " + tClassRead.getI16Field().getValue());
		System.out.println("U32 read is " + tClassRead.getU32Field().getValue());
		System.out.println("I32 read is " + tClassRead.getI32Field().getValue());
		
		System.out.println("I8Array(0) read is " + tClassRead.getI8FieldArray().get(0).getValue());
		System.out.println("I8Array(1) read is " + tClassRead.getI8FieldArray().get(1).getValue());
		System.out.println("I8Array(2) read is " + tClassRead.getI8FieldArray().get(2).getValue());
		
		System.out.println("U16Array(0) read is " + tClassRead.getU16FieldArray().get(0).getValue());
		System.out.println("U16Array(1) read is " + tClassRead.getU16FieldArray().get(1).getValue());
		System.out.println("U16Array(2) read is " + tClassRead.getU16FieldArray().get(2).getValue());
		System.out.println("U16Array(3) read is " + tClassRead.getU16FieldArray().get(3).getValue());
		
		System.out.println("I32Array(0) read is " + tClassRead.getI32FieldArray().get(0).getValue());
		System.out.println("I32Array(1) read is " + tClassRead.getI32FieldArray().get(1).getValue());
		System.out.println("I32Array(2) read is " + tClassRead.getI32FieldArray().get(2).getValue());
		System.out.println("I32Array(3) read is " + tClassRead.getI32FieldArray().get(3).getValue());
		System.out.println("I32Array(4) read is " + tClassRead.getI32FieldArray().get(4).getValue());
		
	}
}
