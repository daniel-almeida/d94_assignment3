package util;

public class U32 {
	
	private final long value;
	
	public U32(long value) {
		this.value = value & 0xFFFFFFFFL;
	}

	public long getValue() {
		return value;
	}

}
