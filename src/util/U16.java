package util;

public class U16 {
	
	private final int value;

	public U16(int value) {
		this.value = value & 0xFFFF;
	}

	public int getValue() {
		return value;
	}
}
