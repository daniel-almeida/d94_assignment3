package util;

public class I16 {
	
	private final short value;

	public I16(short value) {
		this.value = value;
	}

	public short getValue() {
		return value;
	}

}
