package util;

public class I32 {

	private final int value;

	public I32(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

}
